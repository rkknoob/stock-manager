<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes([
    'register' => false,
]);

// Change password
Route::get('password-change', 'Auth\PasswordController@changePassword')->name('password.change');
Route::post('password-change', 'Auth\PasswordController@postChangePassword');

// Home page
Route::get('/', 'HomeController@index')->name('home');

// Product management
Route::resource('products', 'ProductController')->middleware('auth');

// Tracking
Route::resource('tracking', 'TrackingController')->middleware('auth');

// Control Panel
Route::resource('users', 'UserController')->middleware('auth');

// File holder
Route::post('files', 'FileController@store')->middleware('auth');


Route::get('reports/chart-quantity', 'ReportController@chartQuantity')->middleware('auth');
Route::get('reports/top-customers', 'ReportController@topCustomers')->middleware('auth');
Route::get('reports/top-products', 'ReportController@topProducts')->middleware('auth');

Route::get('reports/low-prod', 'ReportController@lowProdudct')->middleware('auth'); //ปั่น

