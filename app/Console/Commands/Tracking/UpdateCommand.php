<?php

namespace App\Console\Commands\Tracking;

use App\Jobs\UpdateTrackingStatusJob;
use Illuminate\Console\Command;

class UpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all tracking status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        UpdateTrackingStatusJob::dispatch();
    }
}
