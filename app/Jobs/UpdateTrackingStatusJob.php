<?php

namespace App\Jobs;

use App\Data\Models\Tracking;
use App\Features\ThailandPost\Contracts\BarcodeTracker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Features\ThailandPost\Constants;
use Illuminate\Support\Facades\Storage;

class UpdateTrackingStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param BarcodeTracker $tracker
     * @return void
     */
    public function handle(BarcodeTracker $tracker)
    {
        Tracking::query()
            ->whereNotNull('tracking_no')
            ->where(function($query) {
                $query->whereNull('current_delivery_status');
                $query->orWhereNotIn('current_delivery_status', Constants\TrackingStatus::allCompleted());
            })
            ->chunk(1000, function(Collection $tracking) use ($tracker) {

                $codes = $tracking->pluck('tracking_no');

                // Call API of Thailand Post
                $items = $tracker->forCode($codes)->get();

                $this->eachForUpdate($tracking, $items);
            });
    }

    /**
     * @param Collection $tracking
     * @param \Illuminate\Support\Collection $items
     */
    private function eachForUpdate(Collection $tracking, $items)
    {
        /** @var \App\Data\Models\Tracking $model */
        foreach ($tracking as $model) {

            if (! isset($items[$model->tracking_no])) {
                continue;
            }

            // All line of the current tracking
            $lines = collect($items[$model->tracking_no]);

            if ($lines->isEmpty()) {
                continue;
            }

            /** @var \App\Features\ThailandPost\LineEntity $lastLine */
            $lastLine = $lines->last();

            // Ignore if current status is not update
            if ($lastLine->status == $model->current_delivery_status) {
                continue;
            }

            // Update new status
            $model->fill([
                'current_delivery_status' => $lastLine->status,
                'current_delivery_data' => $lines->toArray()
            ])->save();
        }
    }
}
