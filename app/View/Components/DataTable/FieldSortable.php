<?php

namespace App\View\Components\DataTable;

use Illuminate\Http\Request;
use Illuminate\View\Component;

class FieldSortable extends Component
{
    /**
     * Field name
     *
     * @var string
     */
    public $name;

    /**
     * Create a new component instance.
     *
     * @param $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.data-table.field-sortable');
    }

    /**
     * @return bool
     */
    public function isCurrentField()
    {
        return request()->get('sort') === $this->name;
    }

    /**
     * @return string
     */
    public function createLinkForSorting()
    {

        return request()->url().'?'.http_build_query($this->newParameters());
    }

    /**
     * @return array
     */
    public function newParameters()
    {
        $params = request()->except(['sort', 'sort_by']);

        $params['sort'] = $this->name;

        if ($this->isCurrentField() && $this->isSortingByAscending()) {
            $params['sort_by'] = 'desc';
        }
       
        return $params;
    }


    /**
     * @return bool
     */
    public function isSortingByAscending()
    {
        return ! request()->get('sort_by')
            || request()->get('sort_by') === 'asc';
    }

    /**
     * @return bool
     */
    public function isSortingByDescending()
    {
        return request()->get('sort_by') === 'desc';
    }
}
