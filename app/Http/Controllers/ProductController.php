<?php namespace App\Http\Controllers;

use App\Data\Models\Bucket;
use App\Data\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


class ProductController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $filter = $request->validate([
            'q' => 'nullable|max:200',
            'sort' => 'nullable',
            'sort_by' => 'nullable|in:asc,desc'
        ]);

        $query = Product::query()
            ->select('pos_item.item_no','pos_item.description','pos_stock.on_hand1','redeem.rqty','pos_item.item_image_name','pos_item.price')
            ->leftJoin('pos_stock', 'pos_stock.item_id', '=', 'pos_item.id')
            ->leftJoinSub(
                "SELECT item_id, SUM(qty) AS rqty FROM pos_sys_so_l GROUP BY item_id",
                "redeem",
                "redeem.item_id", "=", "pos_item.id"
            );

        $query = $query->where('enabled', 1);

        if ($filter['q'] ?? false) {
            $query->where('description', 'LIKE', "%{$filter['q']}%")
                ->orWhere('item_no', $filter['q']);
        }

        $this->withSortingColumnRequest($query, $filter);

        $products = $query->paginate();

        $products = ProductResource::collection($products);

        return view('products.index', compact('products'));
    }

    /**
     * @param Product $product
     * @param Request $request
     * @throws \Exception
     */
    public function update(Product $product, Request $request)
    {
        $data = $request->validate([
            'action' => 'required|in:change-picture',
            'media' => [
                'required',
                Rule::exists('media', 'id')
                    ->where('model_type', (new Bucket)->getMorphClass())
                    ->where('model_id', Bucket::of('holder')->getKey())
            ]
        ]);

        switch ($data['action']) {

            case 'change-picture':

                // Need to move from media path to public path
                // (For support another program to concat the url)

                /** @var Media $media */
                $media = Media::find($data['media']);

                // Ensure media disk must be public disk
                if ($media->disk === 'public') {

                    $oldFilePath = "{$media->getKey()}/{$media->file_name}";

                    $newFilename = time().'.'.$media->getExtensionAttribute();
                    $directory = "products/{$product->getKey()}";

                    // Clean old file
                    if (Storage::disk('public')->exists($directory)) {
                        Storage::disk('public')->deleteDirectory($directory);
                    }

                    // Copy from media location to new location
                    Storage::disk('public')->copy($oldFilePath, "{$directory}/{$newFilename}");

                    $product->fill([
                        'item_image_name' => $newFilename
                    ])->save();

                    // Clean media file
                    $media->delete();
                }

                break;
        }
    }

    private function withSortingColumnRequest($query, array $filter)
    {
        if ($filter['sort'] ?? false) {
            $sortBy = $filter['sort_by'] ?? 'asc';

            $fieldSortable = [
                'product-id' => 'item_no',
                'redeem' => 'rqty',
                'qty' => 'on_hand1',
                'price' => 'price'
            ];

            if ($fieldSortable[$filter['sort']] ?? false) {
                $column = $fieldSortable[$filter['sort']];

                return $query->orderBy($column, $sortBy);
            }
        }

        return $query->orderBy('item_no', 'asc');
    }

}
