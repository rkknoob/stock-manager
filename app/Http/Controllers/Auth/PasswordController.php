<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PasswordController extends Controller
{
    /**
     * PasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword(Request $request)
    {
        return view('auth.change-password');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function postChangePassword(Request $request)
    {
        $data = $request->validate([
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if (! Hash::check($data['old_password'], $request->user()->password)) {
            throw ValidationException::withMessages([
                'old_password' => 'รหัสผ่านเดิมไม่ถูกต้อง'
            ]);
        }

        $request->user()->fill([
            'password' => Hash::make($data['password'])
        ])->save();

        return back()->with('success', 'แก้ไขรหัสผ่านสำเร็จ');
    }
}
