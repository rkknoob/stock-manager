<?php namespace App\Http\Controllers;

use App\Data\Models\Bucket;
use App\Http\Resources\MediaResource;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * @param Request $request
     * @return MediaResource
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|image'
        ]);

        $media = Bucket::of('holder')
            ->addMediaFromRequest('file')
            ->toMediaCollection();

        return new MediaResource($media);
    }
}
