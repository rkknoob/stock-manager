<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function chartQuantity(Request $request)
    {
        $filter = $request->validate([
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
        ]);

        $sql = "
            SELECT
                DATE(so_date) AS date,
                SUM(grand_total) AS total
            FROM pos_sys_so
            WHERE DATE(so_date) BETWEEN ? AND ?
            GROUP BY DATE(so_date)
        ";

        return response()->json([
            'data' => DB::connection('stock')->select($sql, [ $filter['from_date'], $filter['to_date'] ])
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function topCustomers(Request $request)
    {
        $filter = $request->validate([
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
        ]);

        $sql = "
            SELECT customer_name, customer_mobile, SUM(grand_total) sum_g
            FROM pos_sys_so so
            WHERE DATE(so.so_date) BETWEEN ? AND ?
            GROUP BY customer_mobile, customer_name
            ORDER BY sum_g DESC
            LIMIT 10;
        ";

        return response()->json([
            'data' => DB::connection('stock')->select($sql, [$filter['from_date'], $filter['to_date']])
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function topProducts(Request $request)
    {
        $filter = $request->validate([
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
        ]);

        $sql = "
            SELECT item_no, description, unit_price, SUM(qty) AS sum_qty, unit_price * SUM(qty) AS total
            FROM pos_sys_so_l sol
            WHERE DATE(sol.so_date) BETWEEN ? AND ?
            GROUP BY sol.item_no, item_no, description, unit_price
            ORDER BY sum_qty DESC
            LIMIT 10;
        ";

        return response()->json([
            'data' => DB::connection('stock')->select($sql, [$filter['from_date'], $filter['to_date']])
        ]);
    }

    public function lowProdudct(Request $request)
    {
        $filter = $request->validate([
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d',
        ]);

        $sql = "
            SELECT item_no, description, unit_price, SUM(qty) AS sum_qty, unit_price * SUM(qty) AS total
            FROM pos_sys_so_l sol
            WHERE DATE(sol.so_date) BETWEEN ? AND ?
            GROUP BY sol.item_no, item_no, description, unit_price
            ORDER BY sum_qty ASC
            LIMIT 10;
        ";

        return response()->json([
            'data' => DB::connection('stock')->select($sql, [$filter['from_date'], $filter['to_date']])
        ]);
    }
}
