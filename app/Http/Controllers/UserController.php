<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::query()->orderByDesc('id')->paginate();

        return view('users.index', compact('users'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', User::class);

        return view('users.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);

        $data = $request->validate([
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'name' => 'required|max:100',
            'password' => 'required|min:6|max:20'
        ]);

        $data['password'] = Hash::make($data['password']);

        $user = new User($data);
        $user->save();

        return redirect()->route('users.index')->with('success', 'สร้างผู้ใช้งานใหม่สำเร็จ');
    }
}
