<?php namespace App\Http\Controllers;

use App\Data\Models\Tracking;
use App\Events\TrackingCreated;
use App\Http\Resources\TrackingResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class TrackingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $filter = $request->validate([
            'page' => 'nullable|numeric|min:1',
            'q' => 'nullable',
            'return_reason' => 'nullable',
            'from_date' => 'nullable|date_format:Y-m-d',
            'to_date' => 'nullable|date_format:Y-m-d',
            'sort' => 'nullable',
            'sort_by' => 'nullable|in:asc,desc'
        ]);
        $query = Tracking::query()
            ->with('line')
            ->selectRaw('pos_sys_so.*, pos_sys_so_l.item_no, pos_sys_so_l.description,pos_excel_l.order_id')
            ->leftJoin('pos_sys_so_l', 'pos_sys_so.id', '=', 'pos_sys_so_l.so_id')
            ->leftJoin('pos_excel', 'pos_excel.id', '=', 'pos_sys_so.excel_id')
            ->leftJoin('pos_excel_l', 'pos_excel_l.id', '=', 'pos_sys_so.excel_l_id');

        if ($filter['q'] ?? false) {
            $keyword = $filter['q'];
            $query->where(function(Builder $query) use ($keyword) {
                $query->where("pos_sys_so.id", $keyword);
                $query->orWhere('pos_sys_so.customer_name', 'like', "%{$keyword}%");
                $query->orWhere("pos_sys_so.so_no", $keyword);
                $query->orWhere("pos_sys_so.tracking_no", $keyword);
                $query->orWhere('pos_sys_so.customer_mobile', 'like', "%{$keyword}%");
                $query->orWhere('pos_sys_so.customer_email', 'like', "%{$keyword}%");
                $query->orWhere('pos_sys_so.customer_postcode', $keyword);
                $query->orWhere('pos_sys_so_l.id', $keyword);
                $query->orWhere('pos_sys_so_l.description', 'like', "%{$keyword}%");

            });
        }

        // Filter by date
        if ($filter['from_date'] ?? false && $filter['to_date'] ?? false) {
            $query->whereBetween(DB::raw('pos_sys_so.so_date'), [
                $filter['from_date'],
                $filter['to_date']
            ]);
        }
        if ($filter['return_reason'] ?? false) {
            $keyword = $filter['return_reason'];
            if($keyword == "นำจ่ายสำเร็จ"){
                $query->where("pos_sys_so.current_delivery_status", "103");
            }
            if($keyword == "นำจ่ายไม่สำเร็จ"){
                $query->where("pos_sys_so.current_delivery_status", "401");
            }
            if($keyword == "รับฝาก"){
                $query->where("pos_sys_so.current_delivery_status", "503");
            }
            if($keyword == "คืนสินค้า"){
                $query->where("pos_sys_so.return_reason", "=",'ไม่มีผู้รับสินค้า');
            }
        }
        $this->withSortingColumnRequest($query, $filter);
        $tracking = $query->paginate();

        return view('tracking.index', compact('tracking'));
    }

    /**
     * @param Tracking $tracking
     * @param Request $request
     * @return TrackingResource
     */
    public function update(Tracking $tracking, Request $request)
    {
        $data = $request->validate([
            'action' => 'required|in:tracking-no,return',
            'tracking_no' => [
                Rule::requiredIf(fn() => $request->get('action') === 'tracking-no')
            ],
            'return_reason' => [
                Rule::requiredIf(fn() => $request->get('action') === 'return')
            ],
            'return_date' => [
                Rule::requiredIf(fn() => $request->get('action') === 'return'),
                'date_format:Y-m-d'
            ]
        ]);

        switch ($data['action']) {

            // Update tracking no
            case 'tracking-no':
                $tracking->fill([
                    'tracking_no' => $data['tracking_no'],
                    'tracking_update_date' => Carbon::now(),
                    'tracking_update_by' => $request->user()->name,
                ]);
                $tracking->trackingUpdatedByUser()->associate($request->user());
                $tracking->save();

                TrackingCreated::dispatch($tracking);

                return new TrackingResource($tracking);

            case 'return':
                $tracking->fill([
                    'return_reason' => $data['return_reason'],
                    'return_date' => $data['return_date'],
                    'return_by' => $request->user()->save(),
                ]);
                $tracking->returnByUser()->associate($request->user());
                $tracking->save();

                return new TrackingResource($tracking);
        }

        throw new \InvalidArgumentException("Invalid action");
    }

    private function withSortingColumnRequest($query, array $filter)
    {
        if ($filter['sort'] ?? false) {
            $sortBy = $filter['sort_by'] ?? 'asc';

            $fieldSortable = [
                'id' => 'so_no',
                'price' => 'grand_total',
                'file_date' => 'order_id',
                'invoice-date' => 'so_date',
                'tracking-date' => 'tracking_update_date',
            ];

            if ($fieldSortable[$filter['sort']] ?? false) {
                $column = $fieldSortable[$filter['sort']];

                return $query->orderBy($column, $sortBy);
            }
        }

        return $query->orderByDesc('pos_sys_so.so_no');
    }
}
