<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'item_no' => $this->item_no,
            'description' => $this->description,
            'on_hand1' =>$this->on_hand1,
            'rqty' =>$this->rqty,
            'price' =>$this->price,
            'item_image_name' => $this->getImageUrl()
        ];
    }
}
