<?php

namespace App\Providers;

use App\Features\ThailandPost\AccessTokenRepository;
use App\Features\ThailandPost\Authentication\AccessToken;
use App\Features\ThailandPost\Contracts\BarcodeTracker as BarcodeTrackerContract;
use App\Features\ThailandPost\Tracker\BarcodeTracker;
use App\Features\ThailandPost\Tracker\FakeTracker;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class ThailandPostServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BarcodeTrackerContract::class, function($app) {
            if ($app->environment('production')) {
                return $app->make(BarcodeTracker::class);
            }

            return $app->make(FakeTracker::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(AccessToken::class, function($app) {
            return $app->make(AccessTokenRepository::class)->findOrCreate();
        });
    }
}
