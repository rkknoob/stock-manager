<?php

namespace App\Listeners\TrackingCreated;

use App\Events\TrackingCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateHookForTracking implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param TrackingCreated $event
     * @return void
     */
    public function handle(TrackingCreated $event)
    {
        //
    }
}
