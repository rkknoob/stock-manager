<?php


namespace App\Data\Models;


use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    protected $table = 'pos_sys_so_l';

    protected $guarded = [];
}
