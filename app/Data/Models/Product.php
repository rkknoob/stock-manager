<?php namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Log;

class Product extends Model
{
    public $timestamps = false;

    protected $connection = 'stock';

    protected $table = 'pos_item';

    protected $fillable = [
        'item_image',
        'item_image_name'
    ];

    protected $appends = [
        'sum_quantity'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stock()
    {
        return $this->hasOne(Stock::class, 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function saleOrders()
    {
        return $this->hasMany(SaleOrder::class, 'item_id');
    }

    public function getSumQuantityAttribute()
    {
        return $this->saleOrders()->sum('qty');
    }

    /**
     * @return string|null
     */
    public function getImageUrl()
    {

        if (! $this->item_image_name) {
            return null;
        }

        return Storage::disk('public')->url("products/{$this->item_no}/{$this->item_image_name}");
    }

}
