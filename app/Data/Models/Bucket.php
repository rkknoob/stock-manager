<?php namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Bucket extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'buckets';

    protected $fillable = [
        'name'
    ];

    /**
     * @param $name
     * @return \App\Data\Models\Bucket|null
     */
    public static function of($name)
    {
        return static::query()->firstOrCreate(compact('name'));
    }
}
