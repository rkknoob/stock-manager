<?php namespace App\Data\Models;

use App\Features\ThailandPost\LineEntity;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tracking
 * @package App\Data\Models
 * @property-read string so_no
 * @property-read string so_date
 * @property-read string customer_name
 * @property-read string customer_address1
 * @property-read string customer_mobile
 * @property-read string customer_postcode
 * @property-read string total
 * @property-read string created_date
 * @property string tracking_no
 * @property string current_delivery_status
 * @property array current_delivery_data
 */
class Tracking extends Model
{
    public $timestamps = false;

    protected $connection = 'stock';

    protected $table = 'pos_sys_so';

    protected $fillable = [
        'tracking_no',
        'tracking_update_date',
        'tracking_update_by',
        'tracking_update_id',
        'return_reason',
        'return_date',
        'return_by',
        'return_by_id',
        'current_delivery_status',
        'current_delivery_data',
    ];

    protected $dates = [
        'created_date',
        'so_date',
        'tracking_update_date',
        'return_date',
        'order_id'
    ];

    protected $casts = [
        'current_delivery_data' => 'json'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function line()
    {
        return $this->hasOne(TrackingLine::class, 'so_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trackingUpdatedByUser()
    {
        return $this->belongsTo(User::class, 'tracking_update_by_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function returnByUser()
    {
        return $this->belongsTo(User::class, 'return_by_id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getDeliveryHistory()
    {
        return collect($this->current_delivery_data ?: [])->map(function($item) {
            return LineEntity::fromJson($item);
        });
    }
}
