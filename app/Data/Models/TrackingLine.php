<?php namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingLine extends Model
{
    public $timestamps = false;

    protected $connection = 'stock';

    protected $table = 'pos_sys_so_l';
}
