<?php namespace App\Features\ThailandPost;

use App\Features\ThailandPost\Authentication\AccessToken;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class AccessTokenRepository
{
    /**
     * @return AccessToken
     */
    public function findOrCreate(): AccessToken
    {
        if (! $this->hasToken() || ! $this->isTokenValid()) {

            // Json is { token: '', expire: '' }
            $json = $this->call()->json();

            // Token Expire date
            // Sub a day, Cuz we need to renew before token expire
            $expire = Carbon::parse(data_get($json, 'expire'))->subDays(2);

            Cache::remember($this->getCacheIdentifier(), $expire, function() use ($json) {
                return $json;
            });
        }

        return tap(new AccessToken, function(AccessToken $accessToken) {
            $json = $this->getCacheData();

            $accessToken->setExpire(Carbon::parse($json['expire']));
            $accessToken->setToken($json['token']);
        });
    }

    /**
     * @return AccessToken
     */
    public function forceUpdate(): AccessToken
    {
        // Json is { token: '', expire: '' }
        $json = $this->call()->json();

        // Token Expire date
        // Sub a day, Cuz we need to renew before token expire
        $expire = Carbon::parse(data_get($json, 'expire'))->subDays(2);

        if (Cache::has($this->getCacheIdentifier())) {
            Cache::forget($this->getCacheIdentifier());
        }

        Cache::remember($this->getCacheIdentifier(), $expire, function() use ($json) {
            return $json;
        });

        return tap(new AccessToken, function(AccessToken $accessToken) {
            $json = $this->getCacheData();

            $accessToken->setExpire(Carbon::parse($json['expire']));
            $accessToken->setToken($json['token']);
        });
    }


    /**
     * @return string
     */
    public function getCacheIdentifier()
    {
        return 'thailand-post:token';
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return config('services.thailand-post.token');
    }

    /**
     * @return \Illuminate\Http\Client\Response
     */
    private function call()
    {
        return Http::withToken($this->getAuthToken(), 'Token')
            ->acceptJson()
            ->get('https://trackapi.thailandpost.co.th/post/api/v1/authenticate/token');
    }

    /**
     * @return bool
     */
    private function isTokenValid()
    {
        return in_array(['token', 'expire'], array_keys($this->getCacheData()));
    }

    /**
     * @return array
     */
    private function getCacheData()
    {
        $data = Cache::get($this->getCacheIdentifier());

        if (is_string($data)) {
            $data = json_decode($data, true);
        }

        return $data;
    }

    /**
     * @return bool
     */
    private function hasToken()
    {
        return Cache::has($this->getCacheIdentifier());
    }

}
