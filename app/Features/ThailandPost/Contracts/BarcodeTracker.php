<?php namespace App\Features\ThailandPost\Contracts;


use Illuminate\Support\Collection;

interface BarcodeTracker
{
    /**
     * @param $code
     * @return \App\Features\ThailandPost\Contracts\BarcodeTracker
     */
    public function forCode($code): BarcodeTracker;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function get(): Collection;
}
