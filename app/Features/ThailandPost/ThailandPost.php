<?php namespace App\Features\ThailandPost;

use App\Features\ThailandPost\Authentication\AccessToken;
use App\Features\ThailandPost\Contracts\BarcodeTracker;

class ThailandPost
{

    /**
     * @return BarcodeTracker
     */
    public static function tracking()
    {
        return app(BarcodeTracker::class);
    }

    public static function getAccessToken(): AccessToken
    {
        return app(AccessToken::class);
    }
}
