<?php namespace App\Features\ThailandPost\Tracker;

use App\Features\ThailandPost\Authentication\AccessToken;
use App\Features\ThailandPost\LineEntity;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use App\Features\ThailandPost\Contracts\BarcodeTracker as Contract;

class BarcodeTracker implements Contract
{

    /**
     * @var AccessToken
     */
    private AccessToken $accessToken;

    /**
     * @var string[]
     */
    private array $codes = [];

    private array $languages = ['TH'];

    private string $status = 'all';

    /**
     * BarcodeTracker constructor.
     * @param AccessToken $accessToken
     */
    public function __construct(AccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @param $code
     * @return $this
     */
    public function forCode($code): Contract
    {
        $this->codes = $code instanceof Collection ? $code->all() : (array)$code;

        return $this;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function get(): Collection
    {
        $response = Http::withToken($this->accessToken->token, 'Token')
            ->asJson()
            ->acceptJson()
            ->post('https://trackapi.thailandpost.co.th/post/api/v1/track', [
                'barcode' => $this->codes,
                'status' => $this->status,
                'language' => implode(',', $this->languages)
            ]);

        $json = $response->json();

        return collect(data_get($json, 'response.items'))->map(function($lines, $code) {
            return collect($lines)->map(function(array $json) {
                return LineEntity::fromJson($json);
            });
        });
    }
}
