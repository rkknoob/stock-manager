<?php namespace App\Features\ThailandPost\Tracker;

use App\Features\ThailandPost\Contracts\BarcodeTracker as Contract;
use App\Features\ThailandPost\LineEntity;
use Illuminate\Support\Collection;

class FakeTracker implements Contract
{
    /**
     * @param $code
     * @return $this|Contract
     */
    public function forCode($code): Contract
    {
        return $this;
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        $json = @json_decode(file_get_contents(resource_path('json/tracking-response-example.json')), true);

        return collect(data_get($json, 'response.items'))->map(function($lines, $code) {
            return collect($lines)->map(function(array $json) {
                return LineEntity::fromJson($json);
            });
        });
    }
}
