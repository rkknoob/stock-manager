<?php namespace App\Features\ThailandPost;

use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use JsonSerializable;

/**
 * Class LineEntity
 * @package App\Features\ThailandPost
 * @property-read string barcode
 * @property-read string status
 * @property-read string status_description
 * @property-read string status_date
 * @property-read string location
 * @property-read string postcode
 * @property-read string delivery_status
 * @property-read string delivery_description
 * @property-read string delivery_datetime
 * @property-read string receiver_name
 * @property-read string signature
 */
class LineEntity implements Jsonable, JsonSerializable, Arrayable, ArrayAccess
{

    private $data = [];

    /**
     * @param array $json
     * @return static
     */
    public static function fromJson(array $json)
    {
        return tap(app(static::class), function(self $instance) use ($json) {
            $instance->data = $json;
        });
    }

    /**
     * @return array
     */
    public function toBase(): array
    {
        return $this->data;
    }

    /**
     * @param string $dateAttribute
     * @return Carbon|null
     */
    public function dateFor(string $dateAttribute): ?Carbon
    {
        // Date from thailand post is for display only
        // 15/06/2563 10:55:24+07:00
        $date = $this->getAttribute($dateAttribute);

        if ($date) {
            return Carbon::parse($this->convertDateToStandardUnix($dateAttribute));
        }
    }

    /**
     * @param string $dateAttribute
     * @return string|null
     */
    public function convertDateToStandardUnix(string $dateAttribute): ?string
    {
        // 15/06/2563 10:55:24+07:00
        $originalDate = $this->getAttribute($dateAttribute);

        preg_match("/^(\d{2})\/(\d{2})\/(\d{4})\s+(.+)(\+\d{2}:\d{2})$/", $originalDate, $matched);

        [, $days, $months, $years, $time, $timezone] = $matched;

        $years -= 543;

        return "{$years}-{$months}-{$days} {$time}";
    }


    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    /**
     * @param int $options
     * @return false|string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toBase(), $options);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    private function getAttribute($name)
    {
        $value = $this->data[$name] ?? null;

        //

        return $value;
    }



    /**
     * @return false|string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->data;
    }

    public function toArray()
    {
        return $this->toBase();
    }

    public function offsetExists($offset)
    {
        return !!($this->data[$offset] ?? false);
    }

    public function offsetGet($offset)
    {
        return $this->getAttribute($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}
