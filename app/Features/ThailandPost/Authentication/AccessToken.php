<?php namespace App\Features\ThailandPost\Authentication;

use App\Features\ThailandPost\AccessTokenRepository;
use DateTimeInterface;
use Illuminate\Support\Carbon;

class AccessToken
{
    /**
     * @var string
     */
    public string $token;

    /**
     * @var \Carbon\Carbon
     */
    public $expire;

    /**
     * @param string $token
     * @return $this
     */
    public function setToken(string $token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param DateTimeInterface $dateTime
     * @return $this
     */
    public function setExpire(DateTimeInterface $dateTime)
    {
        $this->expire = Carbon::parse($dateTime);

        return $this;
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->expire->isPast();
    }

    /**
     * @return $this
     */
    public function renew()
    {
        /** @var AccessTokenRepository $repository */
        $repository = resolve(AccessTokenRepository::class);

        $accessToken = $repository->forceUpdate();

        $this->setExpire($accessToken->expire);
        $this->setToken($accessToken->token);

        return $this;
    }
}
