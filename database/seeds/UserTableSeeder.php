<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Administrator',
                'role_id' => 'admin',
                'email' => 'admin@feyverly.com'
            ]
        ];

        foreach ($users as $data) {
            User::create(array_merge($data, [
                'password' => Hash::make('123456123456')
            ]));
        }
    }
}
