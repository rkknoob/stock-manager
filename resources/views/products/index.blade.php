@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <h4>รายการสินค้า</h4>
        <p>
            <span>พบ {{ number_format($products->total()) }} รายการ</span>
            @if(request()->get('q'))
                <span>จากการค้นหา "{{ request()->get('q') }}"</span>
            @endif
        </p>
        <div class="card">
            <div class="card-header d-flex justify-content-end">
                <form action="{{ route('products.index') }}" class="d-flex align-items-center">
                    <input type="text" class="form-control" placeholder="ค้นหาจากชื่อ หรือเลขที่สินค้า" name="q" autocomplete="off" value="{{ request()->get('q') }}">

                    <button type="submit" class="btn btn-outline-primary"><i class="fas fa-search"></i></button>

                    @if(request()->get('q'))
                        <a href="{{ route('products.index') }}" class="btn btn-outline-danger">
                            <i class="fas fa-times"></i>
                        </a>
                    @endif
                </form>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <x-data-table.field-sortable name="product-id">
                                Product ID
                            </x-data-table.field-sortable>
                        </th>
                        <th class="text-center">Picture</th>
                        <th>Name</th>
                        <th>
                            <x-data-table.field-sortable name="redeem">
                                Redeem
                            </x-data-table.field-sortable>
                        </th>
                        <th>
                            <x-data-table.field-sortable name="qty">
                                Qty
                            </x-data-table.field-sortable>
                        </th>
                        <th>
                            <x-data-table.field-sortable name="price">
                                Price
                            </x-data-table.field-sortable>
                        </th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $index => $product)

                        <tr>
                            <td>{{ $product->item_no }}</td>
                            <td>
                                <div class="d-flex justify-content-center align-items-center">
                                    @if($product->item_image_name)
                                        <a href="{{ $product->getImageUrl() }}" target="_blank">
                                            <div style="width: 80px; height: 80px; border: 1px solid #ccc; overflow: hidden" class="d-flex justify-content-center align-items-center">
                                                <b-img src="{{ $product->getImageUrl() }}" width="80" height="80"/>
                                            </div>
                                        </a>
                                    @endif
                                </div>
                            </td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->rqty }}</td>
                            <td>{{ $product->on_hand1 }}</td>
                            <td>{{ number_format($product->price) }}</td>
                            <td>
                                {{--                            <product-edit-dialog :product="{{ json_encode($product->resolve()) }}">--}}
                                {{--                                <template v-slot:activator="{ on }">--}}
                                {{--                                    <button type="button" class="btn btn-sm btn-warning" @click.prevent="on">--}}
                                {{--                                        edit--}}
                                {{--                                    </button>--}}
                                {{--                                </template>--}}
                                {{--                            </product-edit-dialog>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="card-footer d-flex justify-content-end">
                {!! $products->appends(request()->except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
