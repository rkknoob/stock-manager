<div>
    <a href="{{ $createLinkForSorting() }}">
        {{ $slot }}

        @if($isCurrentField())
            @if($isSortingByDescending())
                <i class="fas fa-sort-up"></i>
            @else
                <i class="fas fa-sort-down"></i>
            @endif
        @endif
    </a>
</div>
