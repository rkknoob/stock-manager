<div>
    @if(session('success'))
        <div class="alert alert-success">
            <strong>สำเร็จ</strong>
            <p class="mb-0">{{ session('success') }}</p>
        </div>
    @endif
</div>
