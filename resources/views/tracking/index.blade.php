@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-end">
            <div>
                <h4>Tracking</h4>
                <p>
                    <span>พบ {{ number_format($tracking->total()) }} รายการ</span>

                    @if(count($queryParams = request()->only('q', 'from_date', 'to_date')))
                        <ul>
                            @if($queryParams['q'] ?? false)
                                <li>จากการค้นหา "{{ $queryParams['q'] }}"</li>
                            @endif
                            @if($queryParams['from_date'] ?? false && $queryParams['to_date'] ?? false)
                                <li>ในช่วง "{{ $queryParams['from_date'] }} ถึง {{ $queryParams['to_date'] }}"</li>
                            @endif
                        </ul>
                    @endif
                </p>
            </div>

            <div>

            </div>
        </div>

        <div class="card">
            <div class="card-header d-flex justify-content-end">
                <tracking-filter>
                    <template v-slot:activator="{ on }">
                        <b-btn type="button" size="sm" variant="outline-info" @click.prevent="on">
                            <i class="fas fa-search"></i>
                            <span class="ml-2">แสดงการค้นหา</span>
                        </b-btn>
                    </template>
                </tracking-filter>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <x-data-table.field-sortable name="id">ID</x-data-table.field-sortable>
                        </th>
                        <th>Customer</th>
                        <th>Item</th>
                        <th>
                            <x-data-table.field-sortable name="price">
                                Price
                            </x-data-table.field-sortable>
                        </th>
                        <th>
                            <x-data-table.field-sortable name="file_date">
                                Shell File Date
                            </x-data-table.field-sortable>
                        </th>
                        <th>
                            <x-data-table.field-sortable name="invoice-date">
                                Invoice Date
                            </x-data-table.field-sortable>
                        </th>
                        <th>
                            <x-data-table.field-sortable name="tracking-date">
                                Tracking No
                            </x-data-table.field-sortable>
                        </th>
                        <th>Tracking Status</th>
                        <th>Return</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($tracking as $index => $model)
                        <tr>
                            <td>{{ $model->so_no }}</td>
                            <td>
                                <strong>{{ $model->customer_name }}</strong>
                                <div class="d-flex flex-column">
                                    <div class="d-flex flex-nowrap"><i class="fas fa-phone"></i><small>:{{ $model->customer_mobile }}</small></div>
                                    <div class="d-flex flex-nowrap"><i class="fas fa-at"></i><small>:{{ $model->customer_email }}</small></div>
                                </div>
                            </td>
                            <td>
                                <p class="mb-0"><small>{{ $model->description }}</small></p>
                                <div><small class="text-muted">(Product ID: {{ $model->item_no }})</small></div>
                            </td>
                            <td>{{ number_format($model->grand_total) }}</td>
                            <td>{{ optional($model->order_id)->format('d F Y') }}</td>
                            <td><small>{{ optional($model->so_date)->format('d F Y') }}</small></td>
                            <td>
                                @if ($model->tracking_no)
                                    <div><span>{{ $model->tracking_no }}</span></div>
                                    <div><small class="text-muted">{{ optional($model->tracking_update_date)->format('d M Y, H:i') }}</small></div>
                                    <div>
                                        <small class="text-muted">
                                            <a href="https://track.thailandpost.co.th/?trackNumber={{ $model->tracking_no }}" target="_blank">ตรวจสอบ</a>
                                        </small>
                                    </div>
                                @else
                                    <edit-tracking-no-btn :tracking="{{ json_encode((new \App\Http\Resources\TrackingResource($model))->resolve()) }}">
                                        <template v-slot:activator="{ on }">
                                            <b-btn type="button" @click.prevent="on" size="sm">Edit Tracking</b-btn>
                                        </template>
                                    </edit-tracking-no-btn>
                                @endif
                            </td>
                            <td>
                                @if($model->getDeliveryHistory()->isNotEmpty())
                                    @php
                                        $delivery = $model->getDeliveryHistory()->last();

                                    @endphp
                                    <div><strong>{{ optional($delivery)->status }}</strong></div>
                                    <div><strong>{{ optional($delivery)->status_description }}</strong></div>
                                    <div><strong>{{ $delivery ? optional($delivery->dateFor('status_date'))->format('d F Y, H:i') : '' }}</strong></div>
                                @endif
                            </td>
                            <td>
                                @if ($model->return_reason)
                                    <div><span>{{ $model->return_reason }}</span></div>
                                    <div><small class="text-muted">{{ $model->return_date->format('d M Y, H:i') }}</small></div>
                                @else
                                    <edit-return-btn :tracking="{{ json_encode((new \App\Http\Resources\TrackingResource($model))->resolve()) }}">
                                        <template v-slot:activator="{ on }">
                                            <b-btn type="button" @click.prevent="on" size="sm">Edit Returning</b-btn>
                                        </template>
                                    </edit-return-btn>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="card-footer d-flex justify-content-end">
                {!! $tracking->appends(request()->except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
