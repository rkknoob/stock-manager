<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout- ">
            <ul class="kt-menu__nav ">
            </ul>
        </div>
    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: Notifications -->




        <!--end: Quick Actions -->

        <!--begin:: Languages -->


        <!--end:: Languages -->

        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

                <!--use "kt-rounded" class for rounded avatar style-->
                <div class="kt-header__topbar-user kt-rounded-">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{ Auth::user()->name }}</span>
                    <img alt="Pic" src="{{ asset('assets/shell-logo.png') }}" class="kt-rounded-" />

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                </div>
            </div>

            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile" style="background-image: url({{ asset('assets/media/misc/head_bg_sm.jpg') }})">
                    <div class="kt-user-card__wrapper">
                        <div class="kt-user-card__pic">

                            <!--use "kt-rounded" class for rounded avatar style-->
                            <img alt="Pic" src="{{ asset('assets/shell-logo.png') }}" class="kt-rounded-" />
                        </div>
                        <div class="kt-user-card__details">
                            <div class="kt-user-card__name">{{ Auth::user()->name }}</div>
                        </div>
                    </div>
                </div>
                <ul class="kt-nav kt-margin-b-10">
                    <li class="kt-nav__item">
                        <a href="{{ route('password.change') }}" class="kt-nav__link">
                            <span class="kt-nav__link-icon"><i class="flaticon2-gear"></i></span>
                            <span class="kt-nav__link-text">เปลี่ยนรหัสผ่าน</span>
                        </a>
                    </li>
                </ul>
                <ul class="kt-nav kt-margin-b-10">
                    <li class="kt-nav__custom kt-space-between">
                        <form action="{{ route('logout') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">ออกจากระบบ</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- end:: Header Topbar -->
</div>
