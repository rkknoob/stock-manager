<script src='{{asset('assets/plugins/global/plugins.bundle.js')}}'></script>
<script src='{{asset('assets/js/scripts.bundle.js')}}'></script>
<script src='{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}'></script>
<script src='{{asset('assets/js/pages/dashboard.js')}}'></script>
<script src="{{ mix('js/app.js') }}" defer></script>
