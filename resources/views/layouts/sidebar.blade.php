
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid header_side" id="kt_aside_menu_wrapper" style="background-color: white">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500" style="background-color: #1f2433">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('home') }}" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-start-up"></i><span class="kt-menu__link-text">Dashboard</span></a>

            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('products.index') }}" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-list-2"></i><span class="kt-menu__link-text">Product</span></a>

            </li>

            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('tracking.index') }}" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-position"></i><span class="kt-menu__link-text">Tracking</span></a>

            </li>

            @can('viewControlPanel')
                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <a href="{{ route('users.index') }}" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-position"></i>
                        <span class="kt-menu__link-text">จัดการผู้ใข้งาน</span>
                    </a>
                </li>
            @endcan
        </ul>
    </div>
</div>
