@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4 class="text-center">เปลี่ยนรหัสผ่าน</h4>

                <x-session-alert></x-session-alert>

                <div class="mt-4">
                    <form action="{{ route('password.change') }}" method="post">
                        @csrf

                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>รหัสผ่านเดิม</label>
                                    <input type="password" name="old_password" class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}">
                                    @if($errors->has('old_password'))
                                        <span class="invalid-feedback">{{ $errors->first('old_password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>รหัสผ่านใหม่</label>
                                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>ยืนยันรหัสผ่านใหม่</label>
                                    <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}">
                                    @if($errors->has('password_confirmation'))
                                        <span class="invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="card-footer d-flex justify-content-end">
                                <button type="submit" class="btn btn-success"><i class="fas fa-save"></i><span class="ml-2">ยืนยัน</span></button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
@endsection
