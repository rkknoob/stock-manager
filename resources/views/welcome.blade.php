@extends('layouts.app')

@section('content')
    <div class="container">
        <x-session-alert></x-session-alert>

        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning">
                    <p class="mb-0">โปรดเข้าสู่ระบบก่อนการเข้าใช้งาน</p>
                </div>
            </div>
        </div>
    </div>
@endsection
