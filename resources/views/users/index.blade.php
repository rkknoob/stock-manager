@extends('layouts.template')

@section('content')

    <div class="container-fluid">

        <div class="d-flex justify-content-between">
            <div>
                <h4>รายการผู้ใข้งานระบบ</h4>
                <p>พบ {{ $users->total() }} ท่าน</p>
            </div>

            <div>
                @can('create', App\User::class)
                    <a href="{{ route('users.create') }}" class="btn btn-outline-primary">
                        <i class="fas fa-plus"></i>
                        <span>สร้างผู้ใช้งาน</span>
                    </a>
                @endcan
            </div>
        </div>

        <x-session-alert></x-session-alert>

        <div class="card mt-2">
            <table class="table">
                <thead>
                <tr>
                    <th>#ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->getKey() }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="card-footer d-flex justify-content-end">
                {!! $users->render() !!}
            </div>
        </div>
    </div>
@endsection
