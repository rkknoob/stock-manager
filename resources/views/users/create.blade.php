@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <h4>สร้างผู้ใช้งาน</h4>

        <div class="row justify-content-center justify-content-md-start mt-2">
            <div class="col-md-6 col-11">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('users.store') }}" method="post" class="mt-4">
                            @csrf
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>ชื่อ</label>
                                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') }}">
                                        @if($errors->has('name'))
                                            <span class="invalid-feedback">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>อีเมล</label>
                                        <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}">
                                        @if($errors->has('email'))
                                            <span class="invalid-feedback">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>รหัสผ่าน</label>
                                        <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" value="{{ old('password') }}">
                                        @if($errors->has('password'))
                                            <span class="invalid-feedback">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success"><i class="fas fa-plus"></i><span class="ml-2">ยันยัน</span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
