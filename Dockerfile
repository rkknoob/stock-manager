FROM docker.io/parsilver/php-7.4:composer as optimize

ARG ENV_FILE=.env.prod

WORKDIR /var/www/project

COPY . /var/www/project

COPY $ENV_FILE /var/www/project/.env

# Build app
RUN composer install --no-dev --no-suggest --no-interaction \
    && php artisan optimize



# --------------------------
# Front end
FROM docker.io/library/node:14 as frontend

WORKDIR /var/www/project

COPY --from=optimize /var/www/project /var/www/project

RUN npm i && npm run prod && rm -rf node_modules


# --------------------------
# Web service
FROM docker.io/parsilver/php-7.4:apache

WORKDIR /var/www/project

COPY --from=frontend /var/www/project /var/www/project

RUN chmod -R 777 /var/www/project/storage \
    && chmod -R 777 /var/www/project/bootstrap/cache

